package nodes;

import store.*;

public class AssignToVariable extends Node {
	String variableName;
	Node expression;
	
	public AssignToVariable(String variableName, Node expression) {
		this.variableName = variableName;
		this.expression = expression;
	}

	@Override
	public Void run() throws Exception {
		if(Store.getVariable(variableName) == null) {
			Store.addVariable(new Variable(variableName), false);
		}
		Store.setVariable(variableName, expression.run());
		return null;
	}
}
