package nodes;

import java.util.ArrayList;

import store.Store;

public class InputArray extends Node {
	public String name;
	ArrayList<Node> indices;

	public InputArray(String name, ArrayList<Node> indices) {
		this.name = name;
		this.indices = indices;
	}
	
	@Override
	public Void run() throws Exception {
		ArrayList<Integer> realIndices = new ArrayList<>();
		for(Node node : indices) {
			realIndices.add(((Number) node.run()).intValue());
		}
		Store.inputArrayElement(name, realIndices);
		return null;
	}
}
