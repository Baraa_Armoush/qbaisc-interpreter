package nodes;

public class WhileLoop extends Node {
	public Node condition;
	public Block body;
	
	public WhileLoop(Node condition, Block body) {
		this.condition = condition;
		this.body = body;
	}

	@Override
	public Object run() throws Exception {
		while(((Number)condition.run()).intValue() != 0) {
			body.run();
		}
		return null;
	}

}
