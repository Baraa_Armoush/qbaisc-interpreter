package nodes;

public class Print extends Node {
	public Node expr;

	public Print(Node expr) {
		this.expr = expr;
	}

	@Override
	public Object run() throws Exception {
		System.out.println(expr.run());
		// return null;
		// System.out.println("hello");
		// return expr.run();
		return null;
	}
}
