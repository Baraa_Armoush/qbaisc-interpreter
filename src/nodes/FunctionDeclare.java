package nodes;

import store.*;

public class FunctionDeclare extends Node {
	Function function;
	
	public FunctionDeclare(Function function) {
		this.function = function;
	}

	@Override
	public Object run() throws Exception {
		Store.addFunction(function);
		return null;
	}
}
