package nodes;

import java.util.ArrayList;

import store.Array;
import store.Store;

public class ArrayCall extends Node {
	public String name;
	ArrayList<Node> indices;

	public ArrayCall(String name, ArrayList<Node> indices) {
		this.name = name;
		this.indices = indices;
	}
	
	@Override
	public Object run() throws Exception {
		ArrayList<Integer> realIndices = new ArrayList<>();
		for(Node node : indices) {
			realIndices.add(((Number) node.run()).intValue());
		}
		return Store.getArrayElement(name, realIndices);
	}
}
