package nodes;

import java.util.ArrayList;

public class StartNode extends Node {
	private ArrayList<Node> children;
	
	public StartNode() {
		children = new ArrayList<Node>();
	}
	
	public void add(Node node) {
		children.add(node);
	}
	
	@Override
	public Void run() {
		System.out.println(children);
		return null;
	}
}
