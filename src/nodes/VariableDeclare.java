package nodes;

import store.*;

public class VariableDeclare extends Node {
	public Variable variable; 
	public boolean isShared;

	public VariableDeclare(Variable variable, boolean isShared) {
		this.variable = variable;
		this.isShared = isShared;
	}

	@Override
	public Void run() throws Exception {
		Store.addVariable(variable, isShared);
		return null;
	}
}
