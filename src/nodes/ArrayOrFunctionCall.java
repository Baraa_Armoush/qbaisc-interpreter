package nodes;

import java.util.ArrayList;
import store.*;

public class ArrayOrFunctionCall extends Node {
	public String name;
	ArrayList<Node> parameters;

	public ArrayOrFunctionCall(String name, ArrayList<Node> parameters) {
		this.name = name;
		this.parameters = parameters;
	}

	@Override
	public Object run() throws Exception {
		if(!Store.isUsedAsArrayOrFunction(name) ) {
			throw new Exception("undefined array or function " + name);
		}
		if(Store.getArray(name) != null) {
			return new ArrayCall(name, parameters).run();
		} else {
			return new FunctionCall(name, parameters).run();
		}
	}
}
