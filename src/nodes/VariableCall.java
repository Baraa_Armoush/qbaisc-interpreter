package nodes;

import store.*;

public class VariableCall extends Node {
	String name;
	
	public VariableCall(String name) {
		this.name = name;
	}

	@Override
	public Object run() throws Exception {
		return Store.getVariable(name).getValue();
	}
}
