package nodes;

public abstract class Node {
	public abstract Object run() throws Exception;
}
