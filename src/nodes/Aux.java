package nodes;

public class Aux {
	public static Integer compare(Object a, String operator, Object b) throws Exception {
		Class<?> type = unify(a, b);
		Comparable A = (Comparable) cast(a, type);
		Comparable B = (Comparable) cast(b, type);
		int result = A.compareTo(B);
		switch(operator) {
			case "<":	return (result < 0 ? -1 : 0);
			case "<=":	return (result <= 0 ? -1 : 0);
			case ">":	return (result > 0 ? -1 : 0);
			case ">=":	return (result >= 0 ? -1 : 0);
			case "==":	return (result == 0 ? -1 : 0);
			case "<>":	return (result != 0 ? -1 : 0);
		}
		return 0;
	}
	
	public static boolean isNumber(Class<?> a) {
		return a == Short.class || a == Integer.class || a == Float.class || a == Double.class;
	}
	
	// attention: this not writing directly to a, you should reassign the returned value
	public static Object cast(Object a, Class<?> b) throws Exception {
		if(b == String.class) {
			if(a.getClass() != String.class) {
				throw new Exception("Cannot convert " + a.getClass() + " to " + b);
			}
		} else if(isNumber(b)) {
			if(!isNumber(a.getClass())) {
				throw new Exception("Cannot convert " + a.getClass() + " to " + b);
			} else if(b == Short.class) {
				a = ((Number) a).shortValue();
			} else if(b == Integer.class) {
				a = ((Number) a).intValue();
			} else if(b == Float.class) {
				a = ((Number) a).floatValue();
			} else if(b == Double.class) {
				a = ((Number) a).doubleValue();
			}
		} else {
			throw new Exception("unknown type " + b);
		}
		return a;
	}
	
	public static Class<?> unify(Object a, Object b) throws Exception {
		if(a.getClass() == String.class) {
			if(b.getClass() != String.class) {
				throw new Exception("Cannot unify " + a.getClass() + " and " + b);
			}
			return String.class;
		} else if(isNumber(a.getClass())) {
			if(!isNumber(b.getClass())) {
				throw new Exception("Cannot unify " + a.getClass() + " and " + b);
			} else if(a.getClass() == Double.class || b.getClass() == Double.class ) {
				return Double.class;
				// a = ((Number) a).doubleValue();
				// b = ((Number) b).doubleValue();
			} else if(a.getClass() == Float.class || b.getClass() == Float.class) {
				return Float.class;
				// a = ((Number) a).floatValue();
				// b = ((Number) b).floatValue();
			} else if(a.getClass() == Integer.class || b.getClass() == Integer.class) {
				return Integer.class;
				// a = ((Number) a).intValue();
				// b = ((Number) b).intValue();
			} else if(a.getClass() == Short.class || b.getClass() == Short.class) {
				return Short.class;
				// a = ((Number) a).shortValue();
				// b = ((Number) b).shortValue();
			} else {
				throw new Exception("unknown type " + b);
			}
		} else {
			throw new Exception("unknown type " + b);
		}
	}
	
	public static Number add(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return a.shortValue() + b.shortValue();
		} else if(a.getClass() == Integer.class) {
			return a.intValue() + b.intValue();
		} else if(a.getClass() == Float.class) {
			return a.floatValue() + b.floatValue();
		} else if(a.getClass() == Double.class) {
			return a.doubleValue() + b.doubleValue();
		}
		return null;
	}
	
	public static Number sub(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return a.shortValue() - b.shortValue();
		} else if(a.getClass() == Integer.class) {
			return a.intValue() - b.intValue();
		} else if(a.getClass() == Float.class) {
			return a.floatValue() - b.floatValue();
		} else if(a.getClass() == Double.class) {
			return a.doubleValue() - b.doubleValue();
		}
		return null;
	}
	
	public static Number and(Number a, Number b) {
		if(a.getClass() == Short.class && b.getClass() == Short.class) {
			return a.shortValue() & b.shortValue();
		} else {
			return a.intValue() & b.intValue();
		}
	}
	
	public static Number or(Number a, Number b) {
		if(a.getClass() == Short.class && b.getClass() == Short.class) {
			return a.shortValue() | b.shortValue();
		} else {
			return a.intValue() | b.intValue();
		}
	}
	
	public static Number xor(Number a, Number b) {
		if(a.getClass() == Short.class && b.getClass() == Short.class) {
			return a.shortValue() ^ b.shortValue();
		} else {
			return a.intValue() ^ b.intValue();
		}
	}
	
	public static Number mod(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return a.shortValue() % b.shortValue();
		} else if(a.getClass() == Integer.class) {
			return a.intValue() % b.intValue();
		} else if(a.getClass() == Float.class) {
			return a.floatValue() % b.floatValue();
		} else if(a.getClass() == Double.class) {
			return a.doubleValue() % b.doubleValue();
		}
		return null;
	}
	
	public static Number mul(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return a.shortValue() * b.shortValue();
		} else if(a.getClass() == Integer.class) {
			return a.intValue() * b.intValue();
		} else if(a.getClass() == Float.class) {
			return a.floatValue() * b.floatValue();
		} else if(a.getClass() == Double.class) {
			return a.doubleValue() * b.doubleValue();
		}
		return null;
	}
	
	public static Number div(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return a.shortValue() / b.shortValue();
		} else if(a.getClass() == Integer.class) {
			return a.intValue() / b.intValue();
		} else if(a.getClass() == Float.class) {
			return a.floatValue() / b.floatValue();
		} else if(a.getClass() == Double.class) {
			return a.doubleValue() / b.doubleValue();
		}
		return null;
	}
	
	public static Number intDiv(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return a.shortValue() / b.shortValue();
		} else if(a.getClass() == Integer.class) {
			return a.intValue() / b.intValue();
		} else if(a.getClass() == Float.class) {
			return (int) (a.floatValue() / b.floatValue());
		} else if(a.getClass() == Double.class) {
			return (int) (a.doubleValue() / b.doubleValue());
		}
		return null;
	}
	
	public static Number pow(Number a, Number b) throws Exception {
		Class<?> type = unify(a, b);
		a = (Number) cast(a, type);
		b = (Number) cast(b, type);
		if(a.getClass() == Short.class) {
			return (short) Math.pow(a.shortValue(), b.shortValue());
		} else if(a.getClass() == Integer.class) {
			return (int) Math.pow(a.intValue(), b.intValue());
		} else if(a.getClass() == Float.class) {
			return (float) Math.pow(a.floatValue(), b.floatValue());
		} else if(a.getClass() == Double.class) {
			return Math.pow(a.doubleValue(), b.doubleValue());
		}
		return null;
	} 
	
	public static Number not(Number a) throws Exception {
		if(!isNumber(a.getClass())) {
			throw new Exception("unknow type " + a.getClass());
		}
		return ~a.intValue();
	}
}
