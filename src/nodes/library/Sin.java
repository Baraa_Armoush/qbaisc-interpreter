package nodes.library;

import nodes.Node;

public class Sin extends Node {
	Node x;
	
	public Sin(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.sin(((Number) x.run()).doubleValue());
	}
}
