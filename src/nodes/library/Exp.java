package nodes.library;

import nodes.Node;

public class Exp extends Node {
	Node x;
	
	public Exp(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.exp(((Number) x.run()).doubleValue());
	}
}
