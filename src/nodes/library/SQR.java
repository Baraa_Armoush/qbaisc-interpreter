package nodes.library;

import nodes.Node;

public class SQR extends Node {
	Node x;
	
	public SQR(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.sqrt(((Number) x.run()).doubleValue());
	}
}
