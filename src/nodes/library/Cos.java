package nodes.library;

import nodes.*;

public class Cos extends Node {
	Node x;
	
	public Cos(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.cos(((Number) x.run()).doubleValue());
	}
}
