package nodes.library;

import nodes.Node;

public class ABS extends Node {
	Node x;
	
	public ABS(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.abs(((Number) x.run()).doubleValue());
	}
}
