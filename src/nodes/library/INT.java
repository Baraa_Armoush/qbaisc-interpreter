package nodes.library;

import nodes.Node;

public class INT extends Node {
	Node x;
	
	public INT(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.floor(((Number) x.run()).doubleValue());
	}
}
