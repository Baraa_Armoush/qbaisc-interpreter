package nodes.library;

import nodes.Node;

public class SGN extends Node {
	Node x;
	
	public SGN(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.signum(((Number) x.run()).doubleValue());
	}
}
