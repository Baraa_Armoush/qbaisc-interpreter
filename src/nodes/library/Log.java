package nodes.library;

import nodes.Node;

public class Log extends Node {
	Node x;
	
	public Log(Node x) {
		this.x = x;
	}

	@Override
	public Object run() throws Exception {
		return Math.log(((Number) x.run()).doubleValue());
	}
}