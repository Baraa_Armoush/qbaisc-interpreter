package nodes;

import java.util.ArrayList;
import store.*;

public class FunctionCall extends Node {
	public String name;
	ArrayList<Node> parameters;

	public FunctionCall(String name, ArrayList<Node> parameters) {
		this.name = name;
		this.parameters = parameters;
	}

	@Override
	public Object run() throws Exception {
		Object result = null;
		Function function = Store.getFunction(name);
		if(function == null) {
			throw new Exception("undefined function " + name);
		}
		if(parameters.size() != function.parameters.size()) {
			throw new Exception("Incompatible number of parameters\n");
		}
		ArrayList<Variable> variables = new ArrayList<>();
		variables.add(new Variable(function.name, function.returnType));
		for(int i = 0; i < parameters.size(); i++) {
			Object value = parameters.get(i).run();
			if(!function.parameters.get(i).variable.type.checkType(value)) {
				throw new Exception("Incompatible parameter type");
			}
			Variable variable = function.parameters.get(i).variable.clone();
			variable.setValue(value);
			variables.add(variable);
		}
		
		try {
			Store.push();
			for(Variable variable : variables) {
				Store.addVariable(variable, false);
			}
			function.body.run();
			result = Store.getVariable(name).getValue();
		} finally {
			Store.pop();
		}
			
		return result;
	}
}
