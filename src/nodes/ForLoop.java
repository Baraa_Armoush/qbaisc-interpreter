package nodes;

import store.*;

public class ForLoop extends Node {
	public String name;
	public Node from;
	public Node to;
	public Node step;
	public Block body;
	
	public ForLoop(String name, Node from, Node to, Node step, Block body) {
		this.name = name;
		this.from = from;
		this.to = to;
		this.step = step;
		this.body = body;
	}

	@Override
	public Object run() throws Exception {
		Number i = (Number) from.run();
		Number j = (Number) to.run();
		Number k = (Number) step.run();
		
		if(!Store.isUsedAsVariable(name)) {
			new VariableDeclare(new Variable(name), false).run();
		}
		
		if(Aux.compare(k, ">", 0) == -1) {
			for(Store.setVariable(name, i); Aux.compare(Store.getVariable(name).getValue(), "<=", j) == -1; Store.setVariable(name, Aux.add((Number) Store.getVariable(name).getValue(), k))) {
				body.run();
			}
		} else {
			for(Store.setVariable(name, i); Aux.compare(Store.getVariable(name).getValue(), ">=", j) == -1; Store.setVariable(name, Aux.add((Number) Store.getVariable(name).getValue(), k))) {
				body.run();
			}
		}
		return null;
	}

}
