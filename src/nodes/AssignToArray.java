package nodes;

import java.util.ArrayList;
import store.*;

public class AssignToArray extends Node {
	String arrayName;
	ArrayList<Node> indices;
	Node expression;

	public AssignToArray(String arrayName, ArrayList<Node> indices, Node expression) {
		this.arrayName = arrayName;
		this.indices = indices;
		this.expression = expression;
	}

	@Override
	public Void run() throws Exception {
		ArrayList<Integer> realIndices = new ArrayList<>();
		for(Node node : indices) {
			realIndices.add(((Number) node.run()).intValue());
		}
		Store.setArrayElement(arrayName, realIndices, expression.run());
		return null;
	}
}
