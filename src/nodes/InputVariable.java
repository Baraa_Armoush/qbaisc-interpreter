package nodes;

import store.*;

public class InputVariable extends Node {
	String name;

	public InputVariable(String name) {
		this.name = name;
	}

	@Override
	public Object run() throws Exception {
		if(!Store.isUsedAsVariable(name)) {
			new VariableDeclare(new Variable(name), false).run();
		}
		Store.inputVariable(name);
		return null;
	}
}
