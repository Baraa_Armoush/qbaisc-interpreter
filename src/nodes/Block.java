package nodes;

import java.util.ArrayList;

public class Block extends Node {
	ArrayList<Node> nodes;
	
	public Block(ArrayList<Node> nodes) {
		this.nodes = nodes;
	}

	@Override
	public Object run() throws Exception {
		for(Node node : nodes) {
			node.run();
		}
		return null;
	}
}
