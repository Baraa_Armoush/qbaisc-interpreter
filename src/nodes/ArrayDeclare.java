package nodes;

import java.util.ArrayList;

import store.*;

public class ArrayDeclare extends Node {
	String name;
	DataType type;
	ArrayList<Node> dimensions;
	boolean isShared;
	
	public ArrayDeclare(String name, ArrayList<Node> dimensions, boolean isShared) {
		this(name, DataType.getType(name), dimensions, isShared);
	}
	
	public ArrayDeclare(String name, DataType type, ArrayList<Node> dimensions, boolean isShared) {
		this.name = name;
		this.type = type;
		this.dimensions = dimensions;
		this.isShared = isShared;
	}

	@Override
	public Void run() throws Exception {
		ArrayList<Integer> realDimensions = new ArrayList<>();
		for(int i = 0; i < dimensions.size(); i++) {
			realDimensions.add(((Number) dimensions.get(i).run()).intValue());
		}
		Store.addArray(new Array(name, type, realDimensions), isShared);
		return null;
	}
}
