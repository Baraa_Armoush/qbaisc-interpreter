package nodes;

import java.util.ArrayList;

public class Condition extends Node {
	ArrayList<Node> conditions;
	ArrayList<Block> blocks;
	
	public Condition(ArrayList<Node> conditions, ArrayList<Block> blocks) {
		this.conditions = conditions;
		this.blocks = blocks;
	}

	@Override
	public Void run() throws Exception {
		for(int i = 0; i < conditions.size(); i++) {
			if(((Number) conditions.get(i).run()).intValue() != 0) {
				blocks.get(i).run();
				break;
			}
		}
		return null;
	}
}
