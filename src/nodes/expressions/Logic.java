package nodes.expressions;

import java.util.ArrayList;

import nodes.*;

public class Logic extends Node {
	ArrayList<Node> children;
	ArrayList<String> operators;
	
	public Logic(ArrayList<Node> children, ArrayList<String> operators) {
		this.children = children;
		this.operators = operators;
	}

	@Override
	public Object run() throws Exception {
		Object result = (Object) children.get(0).run();
		for(int i = 1; i < children.size(); i++) {
			String operator = operators.get(i - 1);
			Number other = (Number) children.get(i).run(); 
			if(operator.equals("AND")) {
				result = Aux.and((Number) result, other);
			} else if(operator.equals("XOR")) {
				result = Aux.xor((Number) result, other);
			} else if(operator.equals("OR")) {
				result = Aux.or((Number) result, other);
			}
		}
		return result;
	}
}
