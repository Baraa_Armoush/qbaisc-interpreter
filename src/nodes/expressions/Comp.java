package nodes.expressions;

import java.util.ArrayList;

import nodes.*;

public class Comp extends Node {
	ArrayList<Node> children;
	ArrayList<String> operators;
	
	public Comp(ArrayList<Node> children, ArrayList<String> operators) {
		this.children = children;
		this.operators = operators;
	}

	@Override
	public Object run() throws Exception {
		Comparable result = (Comparable) children.get(0).run();
		for(int i = 1; i < children.size(); i++) {
			String operator = operators.get(i - 1);
			Comparable other = (Comparable) children.get(i).run();
			result = Aux.compare(result, operator, other);
		}
		return result;
	}
}
