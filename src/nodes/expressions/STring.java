package nodes.expressions;

import nodes.*;

public class STring extends Node {
	String content;
	
	public STring(String content) {
		this.content = content.substring(1, content.length() - 1);
	}

	@Override
	public Object run() throws Exception {
		return content;
	}
}
