package nodes.expressions;

import java.util.ArrayList;

import nodes.*;

public class Modu extends Node {
	ArrayList<Node> children;
	
	public Modu(ArrayList<Node> children) {
		this.children = children;
	}

	@Override
	public Object run() throws Exception {
		Object result = children.get(0).run();
		for(int i = 1; i < children.size(); i++) {
			result = Aux.mod((Number) result, (Number) children.get(i).run());
		}
		return result;
	}
}
