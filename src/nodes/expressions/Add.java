package nodes.expressions;

import java.util.ArrayList;

import nodes.*;

public class Add extends Node {
	ArrayList<Node> children;
	ArrayList<String> operators;
	
	public Add(ArrayList<Node> children, ArrayList<String> operators) {
		this.children = children;
		this.operators = operators;
	}

	@Override
	public Object run() throws Exception {
		Object firstElement = children.get(0).run();
		if(firstElement.getClass() == String.class) {
			String result = (String) firstElement;
			for(int i = 1; i < children.size(); i++) {
				String operator = operators.get(i - 1);
				String string = (String) children.get(i).run();
				if(operator.equals("+")) {
					result += string;
				} else {
					throw new Exception("undefined operator \"-\" for strings");
				}
			}
			return result;
		} else {
			Number result = (Number) firstElement;
			for(int i = 1; i < children.size(); i++) {
				String operator = operators.get(i - 1);
				Number other = (Number) children.get(i).run(); 
				if(operator.equals("+")) {
					result = Aux.add(result, other);
				} else {
					result = Aux.sub(result, other);
				}
			}
			return result;			
		}
	}
}
