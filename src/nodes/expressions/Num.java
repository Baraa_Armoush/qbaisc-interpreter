package nodes.expressions;

import nodes.*;

public class Num extends Node {
	String number;
	
	public Num(String number) {
		this.number = number;
	}

	@Override
	public Float run() throws Exception {
		return Float.parseFloat(number);
	}
}
