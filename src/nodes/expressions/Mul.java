package nodes.expressions;

import java.util.ArrayList;

import nodes.*;

public class Mul extends Node {
	ArrayList<Node> children;
	ArrayList<String> operators;
	
	public Mul(ArrayList<Node> children, ArrayList<String> operators) {
		this.children = children;
		this.operators = operators;
	}

	@Override
	public Object run() throws Exception {
		Object result = (Object) children.get(0).run();
		for(int i = 1; i < children.size(); i++) {
			String operator = operators.get(i - 1);
			Number other = (Number) children.get(i).run(); 
			if(operator.equals("*")) {
				result = Aux.mul((Number) result, other);
			} else if(operator.equals("/")) {
				result = Aux.div((Number) result, other);
			} else {
				result = Aux.intDiv((Number) result, other);
			}
		}
		return result;
	}
}
