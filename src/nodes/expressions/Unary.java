package nodes.expressions;
import nodes.*;

public class Unary extends Node {
	String operator;
	Node child;
	
	public Unary(String operator, Node child) {
		this.operator = operator;
		this.child = child;
	}

	@Override
	public Object run() throws Exception {
		Object result = (Object) child.run();
		if(operator.equals("-")) {
			result = Aux.mul((Number) result, (short) -1);
		} else if(operator.equals("NOT")) {
			result = Aux.not((Number) result);
		}
		return result;
	}
}
