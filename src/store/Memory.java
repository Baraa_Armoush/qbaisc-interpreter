package store;

import java.util.TreeMap;

public class Memory {
	public TreeMap<String, Variable> sharedVariables;
	public TreeMap<String, Array> sharedArrays;
	public TreeMap<String, Function> sharedFunctions;
	public TreeMap<String, Variable> localVariables;
	public TreeMap<String, Array> localArrays;
	
	public Memory() {
		this(new TreeMap<String, Variable>(), new TreeMap<String, Array>(), new TreeMap<String, Function>(),
				new TreeMap<String, Variable>(), new TreeMap<String, Array>());
	}
	
	public Memory(TreeMap<String, Variable> sharedVariables, TreeMap<String, Array> sharedArrays,
			TreeMap<String, Function> sharedFunctions, TreeMap<String, Variable> localVariables,
			TreeMap<String, Array> localArrays) {
		this.sharedVariables = sharedVariables;
		this.sharedArrays = sharedArrays;
		this.sharedFunctions = sharedFunctions;
		this.localVariables = localVariables;
		this.localArrays = localArrays;
	}
	
	@Override
	public Memory clone() {
		return new Memory((TreeMap<String, Variable>)sharedVariables.clone(), 
				(TreeMap<String, Array>)sharedArrays.clone(),
				(TreeMap<String, Function>)sharedFunctions.clone(),
				new TreeMap<String, Variable>(), new TreeMap<String, Array>());
	}
}
