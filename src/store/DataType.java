package store;

import java.util.Scanner;

public enum DataType {
	STRING,
	SINGLE,
	DOUBLE,
	INTEGER,
	LONG;
	
	public Object defaultValue() {
		switch(this) {
		case STRING:
			return "";
		case SINGLE:
			return (float) 0.0;
		case DOUBLE:
			return (double) 0.0;
		case INTEGER:
			return (short) 0;
		case LONG:
			return (int) 0;
		}
		return -1;
	}
	
	public static DataType getType(String name) {
		if(name.endsWith("$")) {
			return STRING;
		} else if(name.endsWith("!")) {
			return SINGLE;
		} else if(name.endsWith("#")) {
			return DOUBLE;
		} else if(name.endsWith("%")) {
			return INTEGER;
		} else if(name.endsWith("&")) {
			return LONG;
		} else {
			return SINGLE;
		}
	}
	
	public Class<?> getTypeAsClass() {
		switch(this) {
		case STRING:
			return String.class;
		case SINGLE:
			return Float.class;
		case DOUBLE:
			return Double.class;
		case INTEGER:
			return Short.class;
		case LONG:
			return Integer.class;
		}
		return null;
	}
	
	public boolean checkType(Object object) {
		return (this == STRING) == (object.getClass() == String.class);
	}
	
	public Object input() {
		Scanner scanner = new Scanner(System.in);
		switch(this) {
		case STRING:
			return scanner.nextLine();
		case SINGLE:
			return scanner.nextFloat();
		case DOUBLE:
			return scanner.nextDouble();
		case INTEGER:
			return scanner.nextShort();
		case LONG:
			return scanner.nextInt();
		}
		return null;
	}
}
