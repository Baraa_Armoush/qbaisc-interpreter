package store;

import java.util.ArrayList;
import nodes.*;

public class Function {
	public String name;
	public DataType returnType;
	public ArrayList<VariableDeclare> parameters;
	public Block body;
	
	public Function(String name, DataType returnType, ArrayList<VariableDeclare> parameters, Node body) {
		this.name = name;
		this.returnType = returnType;
		this.parameters = parameters;
		this.body = (Block) body;
	}
	
	public Function(String name, ArrayList<VariableDeclare> parameters, Node body) {
		this.name = name;
		this.returnType = DataType.getType(name);
		this.parameters = parameters;
		this.body = (Block) body;
	}
}
