package store;

import nodes.Aux;

public class Variable {
	public String name;
	public DataType type;
	private Object value;
	
	public Variable(String name) {
		this(name, DataType.getType(name));
	}
	
	public Variable(String name, DataType type) {
		this(name, type, type.defaultValue());
	}
	
	public Variable(String name, DataType type, Object value) {
		this.name = name;
		this.type = type;
		this.value = value;
	}	
	
	public void setValue(Object value) throws Exception {
		this.value = Aux.cast(value, this.type.getTypeAsClass());
	}
	
	public Object getValue() throws Exception {
		return Aux.cast(value, type.getTypeAsClass());
	}
	
	public void input() {
		value = type.input();
	}

	@Override
	public Variable clone() throws CloneNotSupportedException {
		try {
			return new Variable(name, type, getValue());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
