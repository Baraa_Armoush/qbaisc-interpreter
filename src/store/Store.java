package store;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import nodes.*;
import nodes.library.*;

public class Store {
	private static Stack<Memory> memory = new Stack<>();
	
	public static void init() {
		memory.add(new Memory());
	}
	
	public static void push() {
		memory.add(memory.peek().clone());
	}
	
	public static void pop() {
		memory.pop();
	}
	
	public static Memory getMemory() {
		return memory.peek();
	}
	
	public static boolean isUsedAsVariable(String name) {
		return getMemory().localVariables.containsKey(name) || 
				getMemory().sharedVariables.containsKey(name);
	}
	
	public static boolean isUsedAsArrayOrFunction(String name) {
		return getMemory().localArrays.containsKey(name) || 
				getMemory().sharedArrays.containsKey(name) || 
				getMemory().sharedFunctions.containsKey(name);
	}
	
	public static Variable getVariable(String name) {
		if(getMemory().localVariables.containsKey(name)) {
			return getMemory().localVariables.get(name);
		} else if(getMemory().sharedVariables.containsKey(name)) {
			return getMemory().sharedVariables.get(name);
		} else {
			return null;
		}
	}
	
	public static void addVariable(Variable variable, boolean isShared) throws Exception {
		if(isUsedAsVariable(variable.name)) {
			throw new Exception("name " + variable.name + " is already used, cannot use it to declare a variable");
		}
		if(isShared) {
			getMemory().sharedVariables.put(variable.name, variable);
		} else {
			getMemory().localVariables.put(variable.name, variable);
		}
	}
	
	public static void setVariable(String name, Object value) throws Exception {
		if(getVariable(name) == null) {
			throw new Exception("undefined variable " + name);
		}
		getVariable(name).setValue(value);
	}
	
	public static void inputVariable(String name) throws Exception {
		if(!isUsedAsVariable(name)) {
			throw new Exception("undefined variable " + name + " to input");
		}
		getVariable(name).input();
	}
	
	public static Function getFunction(String name) {
		if(memory.peek().sharedFunctions.containsKey(name)) {
			return memory.peek().sharedFunctions.get(name);
		} else {
			return null;
		}
	}
	
	public static void addFunction(Function function) throws Exception {
		if(isUsedAsArrayOrFunction(function.name)) {
			throw new Exception("name " + function.name + " is already used, cannot use it to declare a function");
		}
		getMemory().sharedFunctions.put(function.name, function);
	}
	
	public static void DeclareLibraryFunctions() throws Exception {
		addFunction(new Function("ABS", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("ABS", new ABS(new VariableCall("x"))))))));
		addFunction(new Function("SGN", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("SGN", new SGN(new VariableCall("x"))))))));
		addFunction(new Function("SQR", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("SQR", new SQR(new VariableCall("x"))))))));
		addFunction(new Function("INT", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("INT", new INT(new VariableCall("x"))))))));
		addFunction(new Function("Sin", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("Sin", new Sin(new VariableCall("x"))))))));
		addFunction(new Function("Cos", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("Cos", new Cos(new VariableCall("x"))))))));
		addFunction(new Function("Exp", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("Exp", new Exp(new VariableCall("x"))))))));
		addFunction(new Function("Log", DataType.SINGLE, 
				new ArrayList<VariableDeclare>(Arrays.asList(new VariableDeclare(new Variable("x"), false))), 
				new Block(new ArrayList<Node>(Arrays.asList(new AssignToVariable("Log", new Log(new VariableCall("x"))))))));
	}
	
	public static Array getArray(String name) {
		if(getMemory().localArrays.containsKey(name)) {
			return getMemory().localArrays.get(name);
		} else if(getMemory().sharedArrays.containsKey(name)) {
			return getMemory().sharedArrays.get(name);
		} else {
			return null;
		}
	}
	
	public static void addArray(Array array, boolean isShared) throws Exception {
		if(isUsedAsArrayOrFunction(array.name)) {
			throw new Exception("name " + array.name + " is already used, cannot use it to declare a array");
		}
		if(isShared) {
			getMemory().sharedArrays.put(array.name, array);
		} else {
			getMemory().localArrays.put(array.name, array);
		}
	}

	public static Object getArrayElement(String name, ArrayList<Integer> indices) throws Exception {
		if(getArray(name) == null) {
			throw new Exception("undefined array " + name);
		}
		return getArray(name).get(indices);
	}
	
	public static void setArrayElement(String name, ArrayList<Integer> indices, Object value) throws Exception {
		if(getArray(name) == null) {
			throw new Exception("undefined array " + name);
		}
		getArray(name).set(indices, value);
	}
	
	public static void inputArrayElement(String name, ArrayList<Integer> indices) throws Exception {
		if(getArray(name) == null) {
			throw new Exception("undefined array " + name + " to input");
		}
		getArray(name).input(indices);
	}
}
