package store;

import java.util.ArrayList;

import nodes.Aux;

public class Array {
	public String name;
	public DataType type;
	public ArrayList<Integer> dimensions; // dimensions of the array
	private Variable[] array; // internal representation of the N-dimensional array
	private int[] multipliers; // used to calculate the index in the internal array
	
	public Array(String name, ArrayList<Integer> dimensions) {
		this(name, DataType.getType(name), dimensions);
	}
	
	public Array(String name, DataType type, ArrayList<Integer> dimensions) {
		this.name = name;
		this.dimensions = dimensions;
		int arraySize = 1;
	    multipliers = new int[dimensions.size()];
	    for (int idx = dimensions.size() - 1; idx >= 0; idx--) {
	    	multipliers[idx] = arraySize;
	    	arraySize *= dimensions.get(idx);
	    }
	    array = new Variable[arraySize];
	    for(int i = 0; i < arraySize; i++) {
	    	array[i] = new Variable(name, type);
	    }
	}
	
	private int getInternalIndex(ArrayList<Integer> indices) throws Exception {
		if(indices.size() != dimensions.size()) {
			throw new Exception("incompatible dimensions in array " + name + ": "+ indices.size() + " != " + dimensions.size());
		}
		int internalIndex = 0;
	    for (int idx = 0; idx < indices.size(); idx++) {
	    	if(1 > indices.get(idx) || indices.get(idx) > dimensions.get(idx)) {
	    		throw new IndexOutOfBoundsException("Index out of range in array: " + name + ", index: " + indices.get(idx) + ", size: " + dimensions.get(idx));
	    	}
	    	internalIndex += (indices.get(idx) - 1) * multipliers[idx];
	    }
	    return internalIndex;
	}
	
	public Object get(ArrayList<Integer> indices) throws Exception {
	    int internalIndex = getInternalIndex(indices);
	    return array[internalIndex].getValue();
	}
	
	public void set(ArrayList<Integer> indices, Object value) throws Exception {
		int internalIndex = getInternalIndex(indices);
		array[internalIndex].setValue(value);
	}
	
	public void input(ArrayList<Integer> indices) throws Exception {
		int internalIndex = getInternalIndex(indices);
		array[internalIndex].input();
	}
}
