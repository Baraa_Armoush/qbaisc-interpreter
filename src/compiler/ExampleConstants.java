/* Generated By:JavaCC: Do not edit this line. ExampleConstants.java */
package compiler;


/**
 * Token literal values and constants.
 * Generated by org.javacc.parser.OtherFilesGen#start()
 */
public interface ExampleConstants {

  /** End of File. */
  int EOF = 0;
  /** RegularExpression Id. */
  int EOL = 5;
  /** RegularExpression Id. */
  int STRING = 6;
  /** RegularExpression Id. */
  int ADD = 7;
  /** RegularExpression Id. */
  int MUL = 8;
  /** RegularExpression Id. */
  int MOD = 9;
  /** RegularExpression Id. */
  int POW = 10;
  /** RegularExpression Id. */
  int LP = 11;
  /** RegularExpression Id. */
  int RP = 12;
  /** RegularExpression Id. */
  int COMMA = 13;
  /** RegularExpression Id. */
  int DIGIT = 14;
  /** RegularExpression Id. */
  int NUM = 15;
  /** RegularExpression Id. */
  int EQU = 16;
  /** RegularExpression Id. */
  int COMP = 17;
  /** RegularExpression Id. */
  int ASSIGN = 18;
  /** RegularExpression Id. */
  int NOT = 19;
  /** RegularExpression Id. */
  int OR = 20;
  /** RegularExpression Id. */
  int XOR = 21;
  /** RegularExpression Id. */
  int AND = 22;
  /** RegularExpression Id. */
  int IF = 23;
  /** RegularExpression Id. */
  int ELSE = 24;
  /** RegularExpression Id. */
  int ELIF = 25;
  /** RegularExpression Id. */
  int WHILE = 26;
  /** RegularExpression Id. */
  int WEND = 27;
  /** RegularExpression Id. */
  int THEN = 28;
  /** RegularExpression Id. */
  int FOR = 29;
  /** RegularExpression Id. */
  int TO = 30;
  /** RegularExpression Id. */
  int STEP = 31;
  /** RegularExpression Id. */
  int NEXT = 32;
  /** RegularExpression Id. */
  int INPUT = 33;
  /** RegularExpression Id. */
  int PRINT = 34;
  /** RegularExpression Id. */
  int END = 35;
  /** RegularExpression Id. */
  int DIM = 36;
  /** RegularExpression Id. */
  int SHARED = 37;
  /** RegularExpression Id. */
  int FUNCTION = 38;
  /** RegularExpression Id. */
  int LETTER = 39;
  /** RegularExpression Id. */
  int SUFFIX_ID = 40;
  /** RegularExpression Id. */
  int ID = 41;

  /** Lexical state. */
  int DEFAULT = 0;

  /** Literal token values. */
  String[] tokenImage = {
    "<EOF>",
    "\" \"",
    "\"\\r\"",
    "\"\\t\"",
    "<token of kind 4>",
    "\"\\n\"",
    "<STRING>",
    "<ADD>",
    "<MUL>",
    "\"MOD\"",
    "\"^\"",
    "\"(\"",
    "\")\"",
    "\",\"",
    "<DIGIT>",
    "<NUM>",
    "<EQU>",
    "<COMP>",
    "\"=\"",
    "\"NOT\"",
    "\"OR\"",
    "\"XOR\"",
    "\"AND\"",
    "\"IF\"",
    "\"ELSE\"",
    "\"ELIF\"",
    "\"WHILE\"",
    "\"WEND\"",
    "\"THEN\"",
    "\"FOR\"",
    "\"TO\"",
    "\"STEP\"",
    "\"NEXT\"",
    "\"INPUT\"",
    "\"PRINT\"",
    "\"END\"",
    "\"DIM\"",
    "\"SHARED\"",
    "\"FUNCTION\"",
    "<LETTER>",
    "<SUFFIX_ID>",
    "<ID>",
  };

}
